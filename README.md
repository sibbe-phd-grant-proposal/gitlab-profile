# Grant work

Here is the work for the RMC, please contact me to request access. Developing this repo is done using
[`quarto`](https://quarto.org/); [`cargo make`](https://github.com/sagiegurari/cargo-make) is used to
contruct all the files. All files are hosted privatly on [`bookdown`](https://bookdown.org/), you can sign up
there and contact me to access the rendered documents. For the poster, some older programmes (`Rmd`) are
needed.
